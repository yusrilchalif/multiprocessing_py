import time
import multiprocessing

proces1_result = []
def calc_proces1(numbers):
    global proces1_result
    for n in numbers:
        #time.sleep(5)
        print('proces1 ' + str(n*n))
        proces1_result.append(n*n)
        print('Penambahan dengan proses' + str(proces1_result))

proces2_result = []
def calc_proces2(numbers):
    global proces2_result
    for n in numbers:
        #time.sleep(5)
        print('proces2 ' + str(n*n*n))
        proces2_result.append(n*n)
        print('Penambahan dengan proses' + str(proces2_result))

if __name__ == "__main__":
    arr = [2,3,8]
    p1 = multiprocessing.Process(target=calc_proces1, args=(arr,))
    p2 = multiprocessing.Process(target=calc_proces2, args=(arr,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    # print('result' + str('proces1_result'))
    print("Selesai!")